//
//  ImageAssignmentTests.swift
//  ImageAssignmentTests
//
//  Created by Madhuri on 08/02/20.
//  Copyright © 2020 Madhuri. All rights reserved.
//

import XCTest
@testable import ImageAssignment

class ImageAssignmentTests: XCTestCase {
    
    var imageDataModel: ImageDataModel?
    var imageDataViewModel: ImageDataViewModel?
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
       let jsonObject: [String: Any] = [
            "title": "Country",
            "rows": [
                ["title": "1",
                "description": "xyz",
                "imageHref": "abc"],
                ["title": "2",
                "description": nil,
                "imageHref": "abc"],
                ["title": nil,
                "description": "xyz",
                "imageHref": "abc"],
                ["title": "4",
                "description": "xyz",
                "imageHref": nil],
            ]
        ]
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonObject)
            let decoder = JSONDecoder()
            let model = try decoder.decode(ImageDataModel.self, from: jsonData)
            imageDataModel = model

        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func testGetImageDataFromAPI() {
        let expectation = XCTestExpectation.init(description: "Successfull API hit")
        
        DataService.shared.requestforImageInfo { (imageDataModel, error) in
            XCTAssertNil(error)
            XCTAssertNotNil(imageDataModel)
            expectation.fulfill()
        }
    }

    func testCheckAPI_Response() {
        let viewModel = ImageDataViewModel(dataService: DataService())
        viewModel.fetchImageDataFromAPI()
        viewModel.didFinishFetch = {
            XCTAssert(true)
        }
        viewModel.didFailWithError = {
            XCTAssert(false)
        }
    }

    override func tearDown() {
            // Put teardown code here. This method is called after the invocation of each test method in the class.
        imageDataModel = nil
    }
        
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}
