//
//  ImageDataViewModel.swift
//  ImageAssignment
//
//  Created by Madhuri on 08/02/20.
//  Copyright © 2020 Madhuri. All rights reserved.
//

import Foundation

class ImageDataViewModel {
    var navigationBarTitle: String?
    var imageDataInfo = [ImageInfo]()
    var errorMessage: String?
    private var dataService: DataService?
    
    private var imageList: ImageDataModel? {
        didSet {
            guard let imageModel = imageList else { return }
            setDataToProperties(model: imageModel)
            self.didFinishFetch?()
        }
    }
    
    func setDataToProperties(model: ImageDataModel) {
        self.navigationBarTitle = model.title
        self.imageDataInfo = model.imageInfo
    }
    
    // MARK: - Closures
    var didFinishFetch: (() -> Void)? // Closure for data fetched successfully
    var didFailWithError: (() -> Void)? // Error is returned after API call
    
    // MARK: - Network Call
    func fetchImageDataFromAPI() {
        // Webservice call for fetching data
        self.dataService?.requestforImageInfo(completion: { (imageList, error) in
            if let error = error {
                self.errorMessage = error
                self.didFailWithError?()
                return
            }
            self.imageList = imageList
        })
    }
    
    // MARK: - Constructor
    init(dataService: DataService) {
        self.dataService = dataService
    }
}
