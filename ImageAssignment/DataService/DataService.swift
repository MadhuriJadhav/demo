//
//  DataService.swift
//  ImageAssignment
//
//  Created by Madhuri on 08/02/20.
//  Copyright © 2020 Madhuri. All rights reserved.
//

import Foundation
import Alamofire

struct DataService {
    // MARK: - Singleton
    static let shared = DataService()
    // MARK: - URLs
    private var dataUrl =  "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json"
    // MARK: - APIService call to fetch data
    func requestforImageInfo(completion: @escaping (ImageDataModel?, String?) -> Void) {
        Alamofire.request(self.dataUrl).validate().responseString { (response) in
            if response.result.isSuccess {
                guard let data = response.value?.data(using: .utf8) else { return }
                do {
                    let decoder = JSONDecoder()
                    // Add data to model
                    let model = try decoder.decode(ImageDataModel.self, from: data)
                    completion(model, nil)
                } catch let error {
                    print(error.localizedDescription)
                    completion(nil, error.localizedDescription)
                }
            } else {
                completion(nil, response.result.error?.localizedDescription)
            }
        }
    }
}
