//
//  ImageVC-Extension+TableViewDataSource.swift
//  ImageAssignment
//
//  Created by Madhuri on 11/02/20.
//  Copyright © 2020 Madhuri. All rights reserved.
//

import UIKit

extension ImageViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.imageDataInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? ImageViewCell else {
            fatalError("Misconfigured cell")
        }
        let imageDataArray = viewModel.imageDataInfo
        
        if let title = imageDataArray[indexPath.row].title {
            cell.titleLabel.text = title
        }
        
        if let description = imageDataArray[indexPath.row].description {
            cell.descriptionLabel.text = description
        }
        
        if let imageHref = imageDataArray[indexPath.row].imageHref {
            let imageUrl = URL(string: imageHref)
            cell.customImageView.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "PlaceHolder.png"))
        }
        return cell
    }
}
